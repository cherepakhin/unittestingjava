package com.ssg.casino.domain;

import junit.framework.TestCase;

import java.util.HashMap;

public class RollDiceGameTest extends TestCase {

    public void testAddBet() {
        RollDiceGame rollDiceGame = new RollDiceGame();
        Player testPlayer = new Player();
        Bet testBet = new Bet(1, 1);
        rollDiceGame.addBet(testPlayer, testBet);
        HashMap<Player, Bet> playersBets = rollDiceGame.getPlayersBets();
        assertEquals(1, playersBets.size());

    }
}